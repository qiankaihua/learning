#include <set>
#include <iostream>

using std::set;

/**
 * 插入 LOG N
 * 查找 LOG N
 * 删除 LOG N
 */

int main() {
    set<int> st;
    st.insert(1);
    st.insert(7);
    st.insert(2);
    st.insert(5);
    st.insert(3);

    // 获取最小值、最大值, 因为 set 是有序的集合
    std::cout << *st.begin() << ' ' << *st.end() << std::endl;
    return 0;
}