#include <vector>
#include <iostream>

using std::vector;

int main() {
    vector<int> v;

    // 这样可以获取vector的底层数组，可以当成普通数组使用
    int *arr_point = v.data();
    for (int i = 0; i < v.size(); ++i) {
        std::cout << arr_point[i] << ' ';
    }
    return 0;
}