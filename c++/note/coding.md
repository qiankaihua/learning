- 用宏禁止无意义的拷贝构造函数和赋值构造函数

```cpp
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
        TypeName(const TypeName&); \
        TypeName& operator=(const TypeName&)

class Foo {
    xxxxx
private:
    DISALLOW_COPY_AND_ASSIGN(Foo);
};
```

- 显式构造函数: 只有一个参数的构造函数，除非是复制构造函数，或者希望提供隐式转换功能，必须声明为显示构造函数

```cpp
class Foo {
public:
    explicit Foo(const std::string& name): _name(name) {};
private:
    std::string _name;
}
```

- 若类定义了虚函数，则必须定义虚析构函数

```cpp
class Base {
public:
    virtual void Foo();
    virtual ~Base();
}
```

- 必须使用namespace，不能在公共区域使用using

- 类型转换

```cpp
static_cast 表示不会造成损失的转换，例如 int -> double
dynamic_cast 基类转为子类
const_cast 去掉 const
reinterpret_cast 转换内存的解释方式
```

- lambda 不使用默认捕获 &, = 等, 因为默认捕获不会延长生命周期

```cpp
auto fun = [n](int x) {return x + n;}
```

7. smart ptr, 禁止使用 auto_ptr(易错)

```cpp
std::unique_ptr<Foo> ptr(new Foo());
std::scoped_ptr<Shape> p;
std::shared_ptr<Shape> p;
```

8. str 相关

```
不要使用 strcpy, strcat, strdup, sprintf 等没有越界检测的函数
不要使用 strncpy, strncat 等实现有问题的函数

可以使用 strlcpy
```

9. 返回值

负值表示失败，非负值表示成功
