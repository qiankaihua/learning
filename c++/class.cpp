// 禁止复制和拷贝构造函数的宏命令
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&) = delete; \
    TypeName& operator=(const TypeName& rhs) = delete; \
    TypeName(const TypeName&& rhs) = default; \
    TypeName& operator=(TypeName&& rhs) = default


#define DELETE_COPY_AND_MOVE(TypeName) \
    TypeName(const TypeName&) = delete; \
    TypeName& operator=(const TypeName& rhs) = delete; \
    TypeName(const TypeName&& rhs) = delete; \
    TypeName& operator=(TypeName&& rhs) = delete

class Demo
{
private:
    // 使用枚举即可在类内定义常量供数组初始化使用。
    // 也可以使用 static
    enum {test = 10};
    int Arr[test];
public:
    Demo();
    ~Demo();
};

Demo::Demo()
{
}

Demo::~Demo()
{
}
