#include "include.h"

int main() {
    char old_str[20], *new_str;
    int len;

    // 复制字符串，可以用 memcpy 代替 strcpy, 这样对于二进制也是安全的
    len = strlen(old_str);
    new_str = new(char[len + 1]);
    memcpy(new_str, old_str, len + 1);

    // 同理，比较字符串可以使用 memcmp
    memcmp(old_str, new_str, len + 1);

    // memmove 类似 memcpy, 但是它可以处理前后字符串内存有重叠的情况，但是这么做的代价是效率比 memcpy 要低
    memmove(new_str, old_str, len + 1);
}