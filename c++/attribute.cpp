#include "include.h"

// 用 attribute packed 取消字节对齐, 64位机器中对 8 字节对齐
struct __attribute__ ((__packed__)) struct_name {
    uint16_t len;
    char buf[];
};

// __attribute__ format属性可以给被声明的函数加上类似printf或者scanf的特征
// 它可以使编译器检查函数声明和函数实际调用参数之间的格式化字符串是否匹配
// (format (archetype, string-index, first-to-check))
// archetype 指定风格，类似 printf, scanf 等
// 第二个参数指 format string 是函数的第几个变量
// 第三个参数指从第几个变量开始进行类型匹配检查
char* sdscatprintf(char* s, const char *fmt, ...) __attribute__((format(printf, 2, 3)));

int main () {
    return 0;
}