#pragma once

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <memory>
#ifndef NOT_AUTO_USING
using std::shared_ptr;
using std::make_shared;
#endif

#include <string>
#ifndef NOT_AUTO_USING
using std::string;
#endif

#include <list>
#ifndef NOT_AUTO_USING
using std::list;
#endif

#include <iostream>
#ifndef NOT_AUTO_USING
using std::cout;
using std::cin;
using std::endl;
#endif
