
#include "include.h"

void func(shared_ptr<int> p3){
    //使用p3
}// p3离开作用域，销毁
void function(shared_ptr<int>, int);
int g();

int main() {
    shared_ptr<string> p1;  //指向string的智能指针
    shared_ptr<list<int>> p2;   //指向int的list的智能指针

    //指向一个值为42的int的shared_ptr
    shared_ptr<int> p3 = make_shared<int>(42); 
    //指向一个值为"9999999999"
    shared_ptr<string> p4 = make_shared<string>(10, '9');
    //指向一个初始化值的int
    shared_ptr<int> p5 = make_shared<int>();

    shared_ptr<int> p6(new int(1024)); //使用直接初始化方式
    // shared_ptr<int> p6 = new int(1024); //错误，只能使用直接初始化

    // reset() 包含两个操作。当智能指针中有值的时候，调用reset()会使引用计数减1.
    // 当调用reset（new xxx())重新赋值时，智能指针首先是生成新对象，然后将就对象的引用计数减1（当然，如果发现引用计数为0时，则析构旧对象），
    // 然后将新对象的指针交给智能指针保管。
    p1.reset(new string("test"));

    // 获取原始指针
    string *string_ptr = p1.get();

    // 数组使用时必须使用第二个参数指定删除函数，否则就会对数组的指针使用delete (正确应该使用 delete[])
    shared_ptr<int> sp(new int[10], [](int *p) {delete[] p;});

    // shared_ptr 使用 atomic 原子变量来实现引用计数的线程安全 但是若出现修改 shared_ptr 修改指向的指针时，则可能出现多线程问题
    // shared_ptr 的引用计数本身是安全且无锁的，但对象的读写则不是
    // https://blog.csdn.net/D_Guco/article/details/80155323
    

    /****************************************
     *              ERROR
     ****************************************/

    // 不要用一个原始指针初始化多个shared_ptr，原因在于，会造成二次销毁，如下所示：
    int *p5 = new int;
    std::shared_ptr<int> p6(p5);
    std::shared_ptr<int> p7(p5);

    // 尽量不要混用普通指针和智能指针
    // 当一个shared_ptr绑定到一个普通指针的时候，我们就将内存管理的任务交给了shared_ptr，我们不应该再用内置指针来管理内存
    int  *a = new int(10);  //危险！x是个普通指针，而不是一个智能指针
    void func(shared_ptr<int>(a)); //危险！内存会被释放
    cout << *a << endl;        //a是个空指针

    // 不要在函数实参中创建shared_ptr。因为C++的函数参数的计算顺序在不同的编译器下是不同的。正确的做法是先创建好，然后再传入。
    function(shared_ptr<int>(new int), g());
    // 禁止通过shared_from_this()返回this指针，这样做可能也会造成二次析构。
    
}

// 避免循环引用。智能指针最大的一个陷阱是循环引用，循环引用会导致内存泄漏。解决方法是AStruct或BStruct改为weak_ptr
struct AStruct;
struct BStruct;

struct AStruct {
    std::shared_ptr<BStruct> bPtr;
    ~AStruct() { cout << "AStruct is deleted!"<<endl; }
};

struct BStruct {
    std::shared_ptr<AStruct> APtr;
    ~BStruct() { cout << "BStruct is deleted!" << endl; }
};

void TestLoopReference()
{
    std::shared_ptr<AStruct> ap(new AStruct);
    std::shared_ptr<BStruct> bp(new BStruct);
    ap->bPtr = bp;
    bp->APtr = ap;
}

// 智能指针在基类和子类直接转换, 需要 public 继承, 并且必须有多态特性, 也就是必须要有虚函数
class CBase: public std::enable_shared_from_this<CBase> 
{
public:
virtual void f(){}//必须有个虚函数才能向上向下转换。
};
typedef std::shared_ptr<CBase> CBasePtr;
class CChild: public CBase
{};
typedef std::shared_ptr<CChild> CChildPtr;

CBasePtr ptrBase = std::make_shared<CBase>();
//CBasePtr ptrBase = CBasePtr(new CBase());
// 向下转换
CChildPtr ptrChild = std::dynamic_pointer_cast<CChild>(ptrBase);
// 向上转换
CBasePtr ptrXXX = ptrChild;
// 普通转换
CChildPtr ptrXX = CChildPtr(dynamic_cast<CChild*>(ptrXXX.get()));