#include "include.h"



// 使用枚举类并且适配流式输出
enum class EnumClass {
    ENUM1,
    ENUM2,
    ENUM3,
};
inline std::ostream& operator<< (std::ostream& out, EnumClass ec) {
    out << std::to_string(static_cast<int>(ec));
    return out;
}