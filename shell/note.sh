# <当天日期> 这种写法10号以前显示的没有前置0，比如5号显示的是 Jul 5 而不是 Jul 05
today_time=$(date -d 'today' | awk '{print $2 " " $3}')

# <当天日期> 这样可以显示 Jul 05
month=$(date -d 'today' | awk '{print $2}')
day=$(date +%d)
today_time="$month $day"

<统计数字sum> awk '{sum += $1};END {print sum}'
<统计时间大于某个值的日志> awk -F'11-22 ' '{print $2}'| awk '$0 > "14:24"'   (日志格式xxx xx-xx xx:xx:xx awk 需要把日期先提取出来)
<awk条件判断> cat test.txt | awk '{if ($1 < 1024) {sum += 1024} else {sum += $1}}; END {print sum}'
<awk计算分位值> cat input | sort -n | awk '{all[NR] = $0} END{print all[int(NR*0.95 - 0.5)]}'
<查看文件a-b行> sed -n "{a},{b}p" filename  (需要测试一下开闭区间
<去除空行> | tr -s '\n'
<查看当前文件夹及子文件夹的文件数> ls -lR|grep "^-"|wc -l 
<查看当前文件夹及子文件夹的文件夹数> ls -lR|grep "^d"|wc -l 

# 提取内容作为下一次grep的参数
grep getCFrs log/access_log.2020011515 | grep stable=fail | awk '{print $16}' | xargs -i bash -c "grep -a {} log/worker.log.wf.2020011515"

# terminal setting
设置行首显示 export PS1='[\[\e[0;33m\]\u@\[\e[0;32m\]\H:\[\e[0;31m\]\w\[\e[0;37m\]]$ ' -> [user@hostname:pwd]
（https://ss64.com/bash/syntax-prompt.html）

# 查看当前目录下所有文件的 MD5 码：
find ./ -type f -print0 | xargs -0 md5sum | sort >md5.txt

# etcd
./bin/etcdctl --endpoints=10.229.29.141:8806 member list -w table
./bin/etcdctl --endpoints=10.229.29.141:8806,10.229.66.32:8022,10.229.48.143:8232 endpoint status -w table

# 获取后台进程pid
$! # 最后执行命令的pid
nohup $BIN_PATH > nohup.log & echo $! > $PID_FILE # 保存 nuhup 启动的进程 pid
