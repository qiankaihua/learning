# learning

- [c++](./c++)
    - [编码风格](./c++/note/coding.md)
    - [智能指针](./c++/smart_ptr)
        - [shard_ptr](./c++/smart_ptr/shard_ptr.cpp)
    - [字符串](./c++/string.cpp)
    - [宏指令](./c++/define.cpp)
    - [attribute命令](./c++/attribute.cpp)
    - [bit操作](./c++/bit)
        - [bit note](./c++/bit/note.md)
    - [class相关](./c++/class.cpp)
    - [stl](./c++/stl)
        - [vector](./c++/stl/vector.cpp)
        - [set](./c++/stl/set.cpp)
    - [枚举](./c++/enum.cpp)
    - [计时](./c++/time.cpp)